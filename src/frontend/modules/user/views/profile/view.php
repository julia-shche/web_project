<?php
/**
 * @var User $user
 * @var User $currentUser
 */

use common\models\User;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

?>

<h3><?= Html::encode($user->username) ?></h3>
<p><?= HtmlPurifier::process($user->about) ?></p>
<hr>

<?= Html::a('Subscribe', ['/user/profile/subscribe', 'id' => $user->id], ['class' => 'btn btn-info']); ?>
<?= Html::a('Unsubscribe', ['/user/profile/unsubscribe', 'id' => $user->id], ['class' => 'btn btn-info']); ?>


<hr>

<?php if ($currentUser): ?>
    <h5>Общие друзья с <?= $user->username ?>:</h5>
    <div class="row">
        <?php foreach ($currentUser->getMutualSubscriptionsTo($user) as $item): ?>
            <div class="col-md-12">
                <?= Html::a($item->username, ['/user/profile/view', 'nickname' => $item->id], ['target' => '_blank']); ?>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif;?>

<button type="button" class="btn btn-success" data-toggle="modal" data-target="#subscribe">
    Subscriptions: <?= $user->countSubscriptions(); ?></button>
<button type="button" class="btn btn-default" data-toggle="modal" data-target="#follow">
    Followers: <?= $user->countFollowers(); ?></button>

<!-- Modal -->
<div id="subscribe" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Список</h4>
            </div>
            <div class="modal-body">
                <?php foreach ($user->getSubscriptions() as $user): ?>
                    <div class="col-md-12">
                        <?= Html::a($user->username, ['/user/profile/view', 'nickname' => $user->id], ['target' => '_blank']); ?>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<!-- Modal -->
<div id="follow" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Список</h4>
            </div>
            <div class="modal-body">
                <?php foreach ($user->getFollowers() as $user): ?>
                    <div class="col-md-12">
                        <?= Html::a($user->username, ['/user/profile/view', 'nickname' => $user->id, ['target' => '_blank']]); ?>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

