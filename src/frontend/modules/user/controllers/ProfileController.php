<?php

namespace frontend\modules\user\controllers;

use common\models\User;
use Faker\Factory;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Profile controller for the `user` module
 */
class ProfileController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['view'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['subscribe', 'unsubscribe'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $nickname
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($nickname)
    {
        $user = $this->findModelByParam($nickname);
        $currentUser = Yii::$app->user->identity;

        return $this->render('view', compact('user', 'currentUser'));
    }

    /**
     * @throws \yii\base\Exception
     */
    public function actionGenerate()
    {
        $faker = Factory::create();

        for ($i = 0; $i < 1000; $i++) {
            $user = new User([
                'username' => $faker->name,
                'email' => $faker->email,
                'about' => $faker->text,
                'nickname' => $faker->regexify('[A-Za-z0-9_]{5,15}'),
                'auth_key' => Yii::$app->security->generateRandomString(),
                'password_hash' => Yii::$app->security->generateRandomString(),
            ]);
            $user->save(false);
        }
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     */
    public function actionSubscribe($id)
    {
        /** @var User $currentUser */
        $currentUser = Yii::$app->user->identity;
        /** @var User $user */
        $user = $this->findModel($id);

        // Подписываемся
        if ($currentUser->followUser($user)) {
            Yii::$app->session->setFlash('success', 'Подписка успешно произведена');
        } else {
            Yii::$app->session->setFlash('error', 'Ошибка подписки');
        }

        return $this->redirect(['profile/view', 'nickname' =>$id]);
    }

    public function actionUnsubscribe($id)
    {
        /** @var User $currentUser */
        $currentUser = Yii::$app->user->identity;
        /** @var User $user */
        $user = $this->findModel($id);

        // Подписываемся
        if ($currentUser->unFollowUser($user)) {
            Yii::$app->session->setFlash('success', 'Отписка успешно произведена');
        } else {
            Yii::$app->session->setFlash('error', 'Отписка подписки');
        }

        return $this->redirect(['profile/view', 'nickname' =>$id]);
    }

    /**
     * @param $id
     * @return array|null|\yii\db\ActiveRecord
     * @throws NotFoundHttpException
     */
    private function findModel($id)
    {
        if (!$user = User::findOne($id)) {
            throw new NotFoundHttpException('user not found');
        }

        return $user;
    }

    /**
     * @param $param
     * @return array|null|\yii\db\ActiveRecord
     * @throws NotFoundHttpException
     */
    private function findModelByParam($param)
    {
        if (!$user = User::find()->where(['nickname' => $param])->orWhere(['id' => $param])->one()) {
            throw new NotFoundHttpException('user not found');
        }

        return $user;
    }
}
